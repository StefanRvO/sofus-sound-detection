#include "ak5720.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/i2s.h"
#include "modules/gpio_utilities/gpio_utilities.h"
#include "esp_err.h"
#include "esp_log.h"
#include "driver/gpio.h"
#include "driver/ledc.h"
#include <math.h>

#ifdef CONFIG_IDF_TARGET_ESP32

#define I2S_NUM 0

//Driver for the AK5720 i2S ADC.
//I2S pins
#define BCK_PIN           18
#define WS_PIN            5
#define DATA_OUT_PIN      -1
#define DATA_IN_PIN       17

//Custom pins
#define GAIN_SELECT_PIN   16
#define FILTER_SELECT_PIN 21
#define POWER_OFF_PIN     19

#define READ_CHUNK_SIZE 128
#define WINDOW

__attribute__((unused)) static const char *TAG = "AK5720";

static void power_off(void);
static void power_on(void);
static void set_gain_0db(void);
static void set_gain_15db(void);
static void set_filter_sharp(void);
void set_filter_delay(void);

static const i2s_config_t i2s_config = {
     .mode = I2S_MODE_SLAVE | I2S_MODE_RX,
     .sample_rate = SAMPLE_RATE / 2,
     .bits_per_sample = I2S_BITS_PER_SAMPLE_32BIT,
     .channel_format = I2S_CHANNEL_FMT_RIGHT_LEFT,
     .communication_format = I2S_COMM_FORMAT_STAND_I2S,
     .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1, // default interrupt priority
     .dma_buf_count = 8,
     .dma_buf_len = READ_CHUNK_SIZE,
     .use_apll = true,
};

static const i2s_pin_config_t pin_config =
{
    .bck_io_num   = BCK_PIN,
    .ws_io_num    = WS_PIN,
    .data_out_num = DATA_OUT_PIN,
    .data_in_num  = DATA_IN_PIN,
};

void ak5720_init(void)
{
    i2s_driver_install(I2S_NUM, &i2s_config, 0, NULL);
    ESP_LOGI("AK5720", "sample rate: %d", i2s_config.sample_rate);
    i2s_set_pin(I2S_NUM, &pin_config);
    gpio_utilities_setup(POWER_OFF_PIN, GPIO_MODE_OUTPUT, 1);
    gpio_utilities_setup(FILTER_SELECT_PIN, GPIO_MODE_OUTPUT, 0);
    gpio_utilities_setup(GAIN_SELECT_PIN, GPIO_MODE_OUTPUT, 0);
    PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO0_U, FUNC_GPIO0_CLK_OUT1);
    REG_WRITE(PIN_CTRL, 0); 


    gpio_config_t io_conf;
    //disable interrupt
    io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
    //set as output mode
    io_conf.mode = GPIO_MODE_OUTPUT;
    //bit mask of the pins that you want to set,e.g.GPIO18/19
    io_conf.pin_bit_mask = 1 << GAIN_SELECT_PIN | 1 << FILTER_SELECT_PIN | 1 << POWER_OFF_PIN;
    //disable pull-down mode
    io_conf.pull_down_en = 0;
    //disable pull-up mode
    io_conf.pull_up_en = 0;
    //configure GPIO with the given settings
    set_filter_sharp();
    gpio_config(&io_conf);
    set_gain_15db();
    power_off();

    power_on();
}

void ak5720_read(ak5720_samples_t *samples)
{
    int32_t buf[READ_CHUNK_SIZE];
    size_t read = sizeof(buf);
    while(read == sizeof(buf))
    {
        read = 0;
        uint32_t free_space = (samples->buf_size - samples->channel_data[0].sample_cnt) * sizeof(int32_t);
        free_space += (samples->buf_size - samples->channel_data[1].sample_cnt) * sizeof(int32_t);
        if(free_space > sizeof(buf))
        {
            free_space = sizeof(buf);
        }
        i2s_read(I2S_NUM, buf, free_space, &read, 0);
        for(uint32_t i = 0; i < read / 4; i++)
        {
            channel_samples_t* channel_data = &samples->channel_data[samples->nxt_channel];
            uint32_t *sample_cnt = &channel_data->sample_cnt;

            float sample = (buf[i] >> 8) / (float)(1U << 23);

            channel_data->sample_buf[*sample_cnt] = sample;

            (*sample_cnt)++;
            samples->nxt_channel++;
            samples->nxt_channel %= 2;
        }
    }
}

void ak5720_flush(void)
{
    int32_t buf[READ_CHUNK_SIZE];
    size_t read = sizeof(buf);
    while(read == sizeof(buf))
    {
        i2s_read(I2S_NUM, buf, sizeof(buf), &read, 0);
    }
}

void power_off(void)
{
    gpio_set_level(POWER_OFF_PIN, 0);
}

void power_on(void)
{
    gpio_set_level(POWER_OFF_PIN, 1);
}

void set_gain_0db(void)
{
    gpio_set_level(GAIN_SELECT_PIN, 0);
}

void set_gain_15db(void)
{
    gpio_set_level(GAIN_SELECT_PIN, 1);
}

void set_filter_sharp(void)
{
    gpio_set_level(FILTER_SELECT_PIN, 0);
}

void set_filter_delay(void)
{
    gpio_set_level(FILTER_SELECT_PIN, 1);
}
#endif