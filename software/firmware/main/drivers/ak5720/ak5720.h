#pragma once

#include <stdint.h>
#define AK5720_CHANNEL_CNT 2
#define AK5720_BUFFER_SIZE 2048
#define AK5720_BUFFER_COUNT 1

#define SAMPLE_RATE 44100
// #define SAMPLE_RATE 5000

typedef float ak5720_buffer_datatype_t;

typedef struct
{
    ak5720_buffer_datatype_t sample_buf[AK5720_BUFFER_SIZE];
    uint32_t sample_cnt;
} channel_samples_t;

typedef struct {
    uint32_t buf_size;
    channel_samples_t channel_data[AK5720_CHANNEL_CNT];
    uint32_t nxt_channel; 
} ak5720_samples_t;


void ak5720_init(void);
void ak5720_read(ak5720_samples_t *samples);
void ak5720_flush(void);