#pragma once
#include <stdbool.h>

void alive_led_driver_init(void);
void alive_led_set(float strength);
