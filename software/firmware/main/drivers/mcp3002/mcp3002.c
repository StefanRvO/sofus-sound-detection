#include "mcp3002.h"
#include <string.h>
#include "driver/spi_master.h"
#include "esp_err.h"
#include "esp_log.h"

#define QUEUE_SIZE 501
#define START_BIT          (1U << 7)
#define SINGLE_ENDED_MODE  (1U << 6)
#define CHANNEL_SHIFT       5
#define MSB_FIRST_TRANSFER (1U << 4)

__attribute__((unused)) static const char *TAG = "mcp3002";

typedef struct {
    spi_device_handle_t device_handle;
    uint32_t channel_cnt;
    uint32_t channel_send_index;
    spi_transaction_t transaction_buffer[QUEUE_SIZE];
    uint_fast16_t transaction_buffer_index;
} mcp3002_data_t;


DRAM_ATTR static mcp3002_data_t mcp3002_data;

static spi_bus_config_t bus_config =
        {
            .max_transfer_sz = 2,
            .quadwp_io_num   = -1,
            .quadhd_io_num   = -1,
        };

static spi_device_interface_config_t device_cnfg = {
        .command_bits       = 0,
        .address_bits       = 0,
        .dummy_bits         = 0,
        .flags              = 0,
        .pre_cb             = NULL,
        .post_cb            = NULL,
        .clock_speed_hz     = 1500000, //15 bit * 100000sampls/s
        .queue_size         = QUEUE_SIZE - 1,
        .mode               = 0,
    };


esp_err_t mcp3002_init(spi_host_device_t spi_host, int mosi_pin, int miso_pin, int sclk_pin, int cs_pin)
{
    bus_config.mosi_io_num   = mosi_pin;
    bus_config.miso_io_num   = miso_pin;
    bus_config.sclk_io_num   = sclk_pin;
    device_cnfg.spics_io_num = cs_pin;


    ESP_LOGI(TAG,"mcp3002 init.. HOST: %u, MOSI: %u, MISO: %u, SCLK: %u, CS_PIN: %u", spi_host, mosi_pin, miso_pin, sclk_pin, cs_pin);
    esp_err_t ret =  ESP_OK;
    memset(&mcp3002_data, 0x00, sizeof(mcp3002_data));
    for(uint32_t i = 0; i < QUEUE_SIZE; i++)
    {
        mcp3002_data.transaction_buffer[i].flags      = SPI_TRANS_USE_RXDATA | SPI_TRANS_USE_TXDATA; //May be optimised by prefilling all buffers.
        mcp3002_data.transaction_buffer[i].length     = 15;
        mcp3002_data.transaction_buffer[i].rxlength   = 10;
    }
    mcp3002_data.channel_cnt = 2;

    ret |= spi_bus_initialize(spi_host, &bus_config, 0);
    if(ret != ESP_OK) return ret;
    ret = spi_bus_add_device(spi_host, &device_cnfg, &mcp3002_data.device_handle);
    return ret;
}

//Put all samples into the given buffer.
//If needed, we could (RAM) optimize this by passing a callback func
//So we don't need to save the samples, but it is probably not necessary.

esp_err_t mcp3002_sample_step(mcp3002_samples_t *samples)
{
    samples->sample_cnt[0] = 0;
    samples->sample_cnt[1] = 0;

    spi_transaction_t* trans_desc; 
    //Empty receive queue
    while(true)
    {
        esp_err_t ret = spi_device_get_trans_result(mcp3002_data.device_handle, &trans_desc, 0);
        if(ret != ESP_OK)
        {
            break;
        }
        uint16_t sample = trans_desc->rx_data[0] & 0x07;
        sample <<= 7;
        sample += trans_desc->rx_data[1] >> 1;
        samples->sample_buf[(uint32_t)trans_desc->user][samples->sample_cnt[(uint32_t)trans_desc->user]] = (sample / 1024.) * 3.3;
        if(++samples->sample_cnt[(uint32_t)trans_desc->user] == samples->buf_size)
        {
            break;
        }
    }
    //Fill transmit queue. We don't have to check that our transaction struct is not already used
    //as our buffer for those are larger than the queue size.
    while(true)
    {
        trans_desc = &mcp3002_data.transaction_buffer[mcp3002_data.transaction_buffer_index];
        trans_desc->user    = (void *)mcp3002_data.channel_send_index;
        trans_desc->tx_data[0]     = START_BIT | SINGLE_ENDED_MODE | MSB_FIRST_TRANSFER | mcp3002_data.channel_send_index << CHANNEL_SHIFT;
        trans_desc->tx_data[1]     = 0;

        if(spi_device_queue_trans(mcp3002_data.device_handle, trans_desc, 0) != ESP_OK)
        {
            break;
        }
        mcp3002_data.channel_send_index = (mcp3002_data.channel_send_index + 1) % mcp3002_CHANNEL_CNT;
        mcp3002_data.transaction_buffer_index = (mcp3002_data.transaction_buffer_index + 1) & QUEUE_SIZE;
    }
    return ESP_OK; //add better error handling!
}