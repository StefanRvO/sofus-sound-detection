#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "tasks/master_task/master_task.h"


void app_main()
{
    xTaskCreatePinnedToCore(master_task, "MASTER_TASK", 10000, NULL, 10, NULL, 0);
}
