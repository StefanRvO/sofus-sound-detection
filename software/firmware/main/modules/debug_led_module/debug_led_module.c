#include "debug_led_module.h"
#include "esp_log.h"

static strand_t* strand;
static strand_t* strand_ptr[1];

__attribute__((unused)) static const char *TAG = "DEBUG_LED";


void debug_led_module_init(strand_t *debug_strand)
{
    ESP_LOGI(TAG, "init");
    strand = debug_strand;
    strand_ptr[0] = strand;
}

void debug_led_module_set_color(pixelColor_t *color)
{
    strand->pixels_actual[0] = *color;
}

void debug_led_module_update(void)
{
    if(!digitalLeds_isDrawing())
    {
        digitalLeds_drawPixels(strand_ptr, 1);
    }
}

void debug_led_module_set_red(void)
{
    pixelColor_t color;
    color.r = 255;
    color.g = 0;
    color.b = 0;
    debug_led_module_set_color(&color);
}

void debug_led_module_set_off(void)
{
    pixelColor_t color;
    color.r = 0;
    color.g = 0;
    color.b = 0;
    debug_led_module_set_color(&color);
}