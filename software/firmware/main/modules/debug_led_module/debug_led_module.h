#pragma once

#include "esp32_digital_led_lib.h"

#include "modules/c_link.h"

C_LINK void debug_led_module_init(strand_t *debug_strand);
C_LINK void debug_led_module_set_color(pixelColor_t *color);
C_LINK void debug_led_module_set_red(void);
C_LINK void debug_led_module_set_off(void);
C_LINK void debug_led_module_update(void);