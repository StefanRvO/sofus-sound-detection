#include "driver/gpio.h"


void gpio_utilities_setup(int gpioNum, int gpioMode, int gpioVal) 
{
    gpio_num_t gpioNumNative = (gpio_num_t)(gpioNum);
    gpio_mode_t gpioModeNative = (gpio_mode_t)(gpioMode);
    gpio_pad_select_gpio(gpioNumNative);
    gpio_set_drive_capability(gpioNumNative, (gpio_drive_cap_t)0);
    gpio_set_direction(gpioNumNative, gpioModeNative);
    gpio_set_level(gpioNumNative, gpioVal);
}
