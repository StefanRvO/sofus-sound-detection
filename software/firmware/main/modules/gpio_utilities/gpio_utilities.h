#pragma once

#include "driver/gpio.h"


void gpio_utilities_setup(int gpioNum, int gpioMode, int gpioVal);