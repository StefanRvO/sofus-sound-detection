#include "light_control_module.h"
#include "esp32_digital_led_lib.h"
#include "modules/debug_led_module/debug_led_module.h"
#include <string.h>
#include "esp_log.h"

#define LEFT_PIN 26
#define RIGHT_PIN 27
#define DEBUG_PIN 15

__attribute__((unused)) static const char *TAG = "LIGHT_CONTROL";


static strand_t  strands[1];
static strand_t* strand_ptrs[1];
static strand_t* strand_debug = &strands[0];

void light_control_module_init(void)
{
    ESP_LOGI(TAG,"INIT LIGHT CONTROL");
    memset(strands, 0x00, sizeof(strands));

    strand_debug->rmtChannel = 2;
    strand_debug->gpioNum = DEBUG_PIN;
    strand_debug->ledType  = LED_WS2812B_V2;
    strand_debug->brightLimit = 255;
    strand_debug->numPixels = 1;
    strand_ptrs[0] = strand_debug;

    digitalLeds_initDriver();
    digitalLeds_addStrands(strand_ptrs, 1);
    debug_led_module_init(strand_debug);
}