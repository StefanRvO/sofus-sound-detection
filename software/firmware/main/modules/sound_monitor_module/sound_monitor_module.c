#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "modules/gpio_utilities/gpio_utilities.h"
#include "drivers/ak5720/ak5720.h"
#include "libs/statistic_lib/statistics_lib.h"
#include "modules/debug_led_module/debug_led_module.h"
#include "drivers/alive_led_driver/alive_led_driver.h"
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include "esp_log.h"


#define ON_SENSITIVIY 0.000800
#define OFF_SENSITVITY 0.000400
#define OUT_PIN 4

static float sum = 0;
static bool first = false;
static ak5720_samples_t samples = {0};
static running_avg_t on_monitor_avg[AK5720_CHANNEL_CNT];
static running_avg_t off_monitor_avg[AK5720_CHANNEL_CNT];
TickType_t first_update = 0;
__attribute__((unused)) static const char *TAG = "SOUND_MONITOR";

static void handle_samples(void);
static void shift_samples(channel_samples_t *channel_data);
static void sense_on(void);
static void sense_off(void);

void sound_monitor_module_init(void)
{
    ak5720_init();
    gpio_utilities_setup(OUT_PIN, GPIO_MODE_OUTPUT, 0);

    memset(&on_monitor_avg, 0x00, sizeof(on_monitor_avg));
    memset(&off_monitor_avg, 0x00, sizeof(off_monitor_avg));
    on_monitor_avg[0].decay_rate = 1. / SAMPLE_RATE;
    on_monitor_avg[1].decay_rate = 1. / SAMPLE_RATE;
    off_monitor_avg[0].decay_rate = 1. / (SAMPLE_RATE * 5);
    off_monitor_avg[1].decay_rate = 1. / (SAMPLE_RATE * 5);

    samples.channel_data[0].sample_cnt = 0;
    samples.channel_data[1].sample_cnt = 0;

    samples.buf_size = AK5720_BUFFER_SIZE;
    first_update = xTaskGetTickCount() + 2000;
    sense_off();
}

void sound_monitor_module_step(void)
{
    if(first || xTaskGetTickCount() < first_update)
    {
        first = false;
        ak5720_read(&samples);
        ak5720_flush();
    }
    else
    {
        ak5720_read(&samples);
        if(samples.channel_data[0].sample_cnt + samples.channel_data[1].sample_cnt >= samples.buf_size * 2)
        {  
            handle_samples();
            ESP_LOGI(TAG, "LEFT: %f, RIGHT: %f, OFF_LEFT %f, OFF_RIGHT %f", on_monitor_avg[0].avg, on_monitor_avg[1].avg, off_monitor_avg[0].avg, off_monitor_avg[1].avg);
        }

        if(on_monitor_avg[0].avg > ON_SENSITIVIY || on_monitor_avg[1].avg > ON_SENSITIVIY)
        {
            sense_on();
        }
        else if(off_monitor_avg[0].avg < OFF_SENSITVITY && off_monitor_avg[1].avg < OFF_SENSITVITY)
        {
            sense_off();
        }

        
        //CAP off average
        for(uint32_t i = 0; i < AK5720_CHANNEL_CNT; i++)
        {
            if(off_monitor_avg[i].avg > 0.05)
            {
                off_monitor_avg[i].avg = 0.05;
            }
        }
    }



}

void handle_samples(void)
{
    for(uint32_t i = 0; i < 2; i++)
    {
        channel_samples_t* channel_data = &samples.channel_data[i];
        ak5720_buffer_datatype_t *samples = channel_data->sample_buf;

        for(uint32_t j = 0; j < AK5720_BUFFER_SIZE/2 + 1; j++)
        {
            float sample_mag = fabsf(samples[j]);
            statistics_lib_update_running_avg(&on_monitor_avg[i], sample_mag);
            statistics_lib_update_running_avg(&off_monitor_avg[i], sample_mag);
            sum += samples[j];

        }
        shift_samples(channel_data);
        channel_data->sample_cnt = 0;
    }
}

void shift_samples(channel_samples_t *channel_data)
{
    for(uint32_t i = 0; i < AK5720_BUFFER_SIZE / 2; i++)
    {
        channel_data->sample_buf[i] = channel_data->sample_buf[i + AK5720_BUFFER_SIZE / 2];
    }
    channel_data->sample_cnt = AK5720_BUFFER_SIZE / 2;
}

void sense_on(void)
{
    gpio_utilities_setup(OUT_PIN, GPIO_MODE_OUTPUT, 1);
    pixelColor_t color;
    color.r = 0;
    color.g = 255;
    color.b = 0;

    debug_led_module_set_color(&color);
}

void sense_off(void)
{
    gpio_utilities_setup(OUT_PIN, GPIO_MODE_OUTPUT, 0);
    pixelColor_t color;
    color.r = 255;
    color.g = 0;
    color.b = 0;
    debug_led_module_set_color(&color);
}