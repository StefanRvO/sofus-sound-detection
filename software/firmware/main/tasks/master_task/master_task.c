#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "esp_log.h"
#include "modules/sound_monitor_module/sound_monitor_module.h"

#include "drivers/alive_led_driver/alive_led_driver.h"
#include "modules/light_control_module/light_control_module.h"
#include "modules/debug_led_module/debug_led_module.h"

#define TASK_INTERVAL 2

__attribute__((unused)) static const char *TAG = "MASTER TASK";


static void handle_alive_led(void);


esp_err_t master_task_init(void)
{
    light_control_module_init();
    alive_led_driver_init();
    sound_monitor_module_init();

    esp_err_t ret = ESP_OK;
    ESP_LOGI(TAG,"MASTER INIT");
    return ret;
}

void master_task(void *taskdata)
{
  ESP_LOGI(TAG,"MASTER LOOP STARTED");
  ESP_ERROR_CHECK(master_task_init());
  TickType_t last_wake_time = xTaskGetTickCount();

  while(true)
  {
    vTaskDelayUntil(&last_wake_time, TASK_INTERVAL);
    handle_alive_led();
    sound_monitor_module_step();
    debug_led_module_update();

  }
}

void handle_alive_led(void)
{
  static TickType_t last_wake_time = 0;
  static float strength = 0;
  if(last_wake_time + 1000 < xTaskGetTickCount())
  {
    last_wake_time = xTaskGetTickCount();
    strength = 1 - strength;
    alive_led_set(strength);
  }
}