#!/usr/bin/python3
import sys
import requests
from requests_toolbelt import MultipartEncoder, MultipartEncoderMonitor
import argparse
import json
import time
import os
FIELD_NAME = "ota"
UPLOAD_URI = "/api/ota"

class OTAUploader:
    def __init__(self, filename, host):
        self.filename = filename
        self.host = host

    def progress(self, count, total, status=''):
        #https://gist.github.com/vladignatyev/06860ec2040cb497f0f3
        bar_len = 60
        filled_len = int(round(bar_len * count / float(total)))

        percents = round(100.0 * count / float(total), 1)
        bar = '=' * filled_len + '-' * (bar_len - filled_len)

        sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', status))
        sys.stdout.flush() # As suggested by Rom Ruben (see: http://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console/27871113#comment50529068_27871113)

    def progress_callback(self, monitor):
        self.progress(monitor.bytes_read, monitor.len, status='Uploading OTA')

    def upload(self, upload_url):
        with open(self.filename, 'rb') as f:
            e = MultipartEncoder(
                fields={'ota': ('ota', f.read())}
                )
            m = MultipartEncoderMonitor(e, self.progress_callback)
            request = requests.post(upload_url, data=m, headers={'Content-Type': e.content_type} )
            print()
            if request.status_code == 200:
                return 0
            else:
                return -1

    def do_ota(self):
        upload_url = "http://" + self.host + UPLOAD_URI
        upload_result = self.upload(upload_url)
        if(upload_result):
            print("Upload failed")
            return -1
        else:
            print("Upload successfull")

def __main__():
    parser = argparse.ArgumentParser(description='Upload firmware using OTA')
    parser.add_argument('-f', '--file', help="The file to flash.")
    parser.add_argument('-ht', '--host', help="The host(s) to upload to.", default = "192.168.0.1", nargs = '+')
    args = parser.parse_args()
    for host in args.host:
        ota = OTAUploader(args.file, host)
        err = ota.do_ota()
        if(err): return err


if __name__ == "__main__":
	__main__()
