import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import SimpleMenu from './SimpleMenu'

export default class MenuAppBar extends React.Component {
  render()
  {
    return (
      <div>
        <AppBar position="static">
          <Toolbar>
            <SimpleMenu pages={this.props.pages}
                        setPage={this.props.setPage}
            />
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}