import React, { Component } from "react";
import FormControl from '@material-ui/core/FormControl';
import { withStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import MESSAGE_TYPES from './Types/MessageTypes'

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        width: '100%',
        margin: 'auto'
      },
      formControl: {
        margin: 'auto',
        width: '90%',
      },
      title: {
        flexGrow: 1,
      },    
  });
  
class GitterPage extends Component {
    constructor(props, context) {
      super(props, context);
      this.handleData = this.handleData.bind(this);
      this.handleOnChange = this.handleOnChange.bind(this);

      this.msgType = MESSAGE_TYPES["GLITTER"];
      
      props.socket.registerHandler(this.msgType, this.handleData);

      this.state = {
        update_rate     : 0,
        random_color    : false,
        percent_on      : 0,
      }
    }

    static getPageName()
    {
        return "Glitter";
    }

    handleData(data)
    {
        this.setState({
            update_rate  : data.readUInt32LE(0),
            random_color : Boolean(data.readUInt8(4)),
            percent_on   : data.readFloatLE(5),
        });
    }

    handleOnChange = name => (event, checked) => 
    {
        var state = this.state;
        state["random_color"] = checked;
        this.setState(state);
        this.publishState();
    }

    handleOnChangeSlider = name => (e, value) => {
        if(name === "update_rate")
        {
            value*= 1000000;
        }
        this.setState({
            [name]: value
        });
        this.publishState();
    };

    publishState()
    {
        var buf = new Buffer(10)
        buf.writeUInt8(this.msgType);
        buf.writeUInt32LE(this.state.update_rate, 1);
        buf.writeUInt8(this.state.random_color, 5);
        buf.writeFloatLE(this.state.percent_on, 6);
        this.props.socket.publish(buf);
    }
      render()
    {
        const { classes } = this.props;
        return(
            <div>
            <Typography align="center" variant="h5">Glitter Mode Settings</Typography>
            <form className={classes.root} autoComplete="off">
            <FormControl className={classes.formControl} margin='normal'>
                <Typography id="update-rate-label" gutterBottom>
                    Update Rate
                </Typography>
                <Slider
                    value={this.state.update_rate / 1000000}
                    onChange={this.handleOnChangeSlider("update_rate")}
                    onChangeCommitted={this.handleOnChangeSlider("update_rate")}
                    aria-labelledby="update-rate-label"
                    step={0.01}
                    min={0.01}
                    max={5}
                    marks
                    name='update_rate'
                    valueLabelDisplay="auto"    
                />           
                <Typography id="percent-on-label" gutterBottom>
                On Percent
                </Typography>
                <Slider
                    value={this.state.percent_on}
                    onChange={this.handleOnChangeSlider("percent_on")}
                    onChangeCommitted={this.handleOnChangeSlider("percent_on")}
                    aria-labelledby="percent-on-label"
                    step={1}
                    min={0}
                    max={100}
                    marks
                    name='percent-on'
                    valueLabelDisplay="auto"    
                />
                <FormControlLabel
                    control={
                    <Checkbox checked={this.state.random_color} onChange={this.handleOnChange('random_color')} value="1" />
                    }
                    label="Random Color"
                />
                </FormControl>
            </form>
            </div>
        );
    }
  }

  export default withStyles(styles)(GitterPage);