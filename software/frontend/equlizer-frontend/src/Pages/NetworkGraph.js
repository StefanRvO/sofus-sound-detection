import React, { Component } from "react";
import Tree from 'react-tree-graph';
import 'react-tree-graph/dist/style.css'
import './NetworkGraph.css'

  class NetworkGraph extends Component {
    constructor(props, context) {
      super(props, context);
      this.buildGraphData = this.buildGraphData.bind(this);

    }

    buildGraphData() {
        var nodes = Object.entries(this.props.nodes)
        var nodeTree = {}
        nodes.forEach(function(item, index) {
            var parent_mac = parseInt(item[1].parent_mac, 16) - 1;
            nodeTree[parent_mac] = {}
            nodeTree[parent_mac].children = []
            nodeTree[parent_mac].mac = item[1].parent_mac
            nodeTree[parent_mac].textProps = {x: -25, y: 25}
        });
        nodes.forEach(function(item, index) {
            var parent_mac = parseInt(item[1].parent_mac, 16) - 1;
            var node_mac   = parseInt(item[1].mac, 16);
            if(node_mac in nodeTree) {
                nodeTree[node_mac].name = item[1].node_name
                nodeTree[parent_mac].children.push(nodeTree[node_mac]);
            }
            else {
                var node = {}
                node.mac = item[1].mac
                node.name = item[1].node_name
                node.textProps = {x: -25, y: 25}
                nodeTree[parent_mac].children.push(node)
            }

        });
        
        //Select root tree, give name and return
        var ret_item = {}
        Object.entries(nodeTree).forEach(function(item, index) {
            if(!item[1].name)
            {
                item[1].name = "router"
                ret_item = item[1];
            }
        });
        return ret_item;
    }

    render()
    {
        let data = this.buildGraphData();
      return(
          <div>
            <Tree
            keyProp="mac"
            data={data}
            height={400}
            width={400}
            nodeRadius={10}
            margins={{ top: 20, bottom: 10, left: 40, right: 200 }}
            animated
            svgProps={{
                className: 'custom'
            }}
            />);
          </div>
      );
    }
  }

  export default NetworkGraph;