import React, { Component } from "react";
import FormControl from '@material-ui/core/FormControl';
import { withStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'; 
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import RingSettings from './RingSettings'

import CHANNEL_ORDER from './Types/ChannelOrder'
import MODES from './Types/Modes'
import LIGHT_TYPES from "./Types/LightTypes";
import COLORORDER from "./Types/ColorOrder";

const styles = theme => ({
    root: {
        width: '100%',
      },
      heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
      },
      table: {
        minWidth: 650,
      },
      textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200,
      },
  });

  class NodeInfo extends Component {
    constructor(props, context) {
      super(props, context);
      this.handleOnChange = this.handleOnChange.bind(this);
      this.handleOnChangeCheckbox = this.handleOnChangeCheckbox.bind(this);
      this.handleOnChangeSelect = this.handleOnChangeSelect.bind(this);



      this.publishState = this.publishState.bind(this);
      this.state =  Object.assign({}, this.props.data)
      this.msgType = props.msgType;
      this.left_ring_state  = React.createRef()
      this.right_ring_state = React.createRef()
    }


    createData(name, value) {
        return { name, value };
    }

    publishState()
    {
        var buf = new Buffer(114)
        buf.writeUInt8(this.msgType);

        buf.write(this.props.data.mac, 1, 7, 'hex');
        buf.write(this.props.data.parent_mac, 7, 13, 'hex');
        buf.writeDoubleLE(0, 13); //Time
        buf.writeFloatLE(0, 21); //Air time
        buf.writeUInt8(this.state.node_name.length, 25);
        buf.write(this.state.node_name, 26, 46, "ascii");
        buf.writeUInt8(0, 46); //Board Type
        buf.writeUInt32LE(0, 47); //Strobe offset
        buf.writeUInt16LE(this.state.led_len_left,    51);
        buf.writeUInt16LE(this.state.led_len_right,   53);
        buf.writeUInt8(this.state.local_mode,         55);
        buf.writeUInt8(this.state.is_mode_global,     56);
        buf.writeUInt8(0, 57); //Layer

        buf.writeUInt8(this.state.left_panel_height,  58);
        buf.writeUInt8(this.state.left_panel_width,   59);
        buf.writeUInt8(this.state.right_panel_height, 60);
        buf.writeUInt8(this.state.right_panel_width,  61);
        buf.writeUInt8(this.left_ring_state.current.state.ring_size, 62)
        for(let i = 0; i < 15; i++)
        {
          buf.writeUInt8(this.left_ring_state.current.state["ring_size_" + i], 63 + i);
        }
        buf.writeUInt8(this.right_ring_state.current.state.ring_size, 82)
        for(let i = 0; i < 15; i++)
        {
          buf.writeUInt8(this.right_ring_state.current.state["ring_size_" + i], 83 + i);
        } 
        buf.writeUInt8(this.state.light_type,        102);
        buf.writeUInt8(this.state.left_color_order,  103);
        buf.writeUInt8(this.state.right_color_order, 104);

        buf.writeUInt8(this.state.channel_order,     113);

        this.props.socket.publish(buf);
    }


    handleOnChange(e, value)  {
        this.setState({
            [e.target.id]: e.target.value,
        }, this.publishState);
    };

    handleOnChangeCheckbox(e, value)  {
        this.setState({
            [e.target.id]: value,
        }, this.publishState);
    };

    handleOnChangeSelect = name => (e, value) => {
        this.setState({
          [name]: value.props.value,
      }, this.publishState);
    };


      render()
    {
      const { classes } = this.props;
      const rows = [
        this.createData('Name', this.props.data.node_name),
        this.createData('Type', this.props.data.board_type),
        this.createData('Firmware Sync', (this.props.data.fw_sync_progress * 100).toFixed(2) ),
        this.createData('Filesystem Sync', (this.props.data.fs_sync_progress * 100).toFixed(2) ),

        this.createData('Strobe Offset', (this.props.data.strobe_offset / 1000).toFixed(0)),
        this.createData('Left Length', this.props.data.led_len_left),
        this.createData('Channel Order', CHANNEL_ORDER[this.props.data.channel_order]),

        this.createData('Left Panel Height', this.props.data.left_panel_height),
        this.createData('Left Panel Width', this.props.data.left_panel_width),

        this.createData('Right Length', this.props.data.led_len_right),
        this.createData('Right Panel Height', this.props.data.right_panel_height),
        this.createData('Right Panel Width', this.props.data.right_panel_width),

        this.createData('Time', this.props.data.time.toFixed(3)),
        this.createData('Mean Air Time', (this.props.data.mean_air_time / 1000).toFixed(1)),
        this.createData('Local Mode', MODES[this.props.data.local_mode]),
        this.createData('Global Mode Enabled', this.props.data.is_mode_global),
        this.createData('Layer', this.props.data.layer),
        this.createData('Parent', this.props.data.parent_mac),
        this.createData('Light Type', LIGHT_TYPES[this.props.data.light_type]),
        this.createData('Left Color Order', COLORORDER[this.props.data.left_color_order]),
        this.createData('Right Color Order', COLORORDER[this.props.data.right_color_order]),
    ]
      const modeitems = Object.keys(MODES).map((value, index) => {
        return(
            <MenuItem key={value} value={value}>{MODES[value]}</MenuItem>
          );
      });

      const lightTypeItems = Object.keys(LIGHT_TYPES).map((value, index) => {
        return(
            <MenuItem key={value} value={value}>{LIGHT_TYPES[value]}</MenuItem>
          );
      });

      const colorOrderItems = Object.keys(COLORORDER).map((value, index) => {
        return(
            <MenuItem key={value} value={value}>{COLORORDER[value]}</MenuItem>
          );
      });

      const channelOrderItems = Object.keys(CHANNEL_ORDER).map((value, index) => {
        return(
            <MenuItem key={value} value={value}>{CHANNEL_ORDER[value]}</MenuItem>
          );
      });


      return(
          <div>
            <ExpansionPanel key={this.props.data.mac}>
                <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls={this.props.data.mac + "-content"}
                id={this.props.data.mac + "-header"}
                >
                <Typography className={classes.heading}>{this.props.data.node_name} -- {this.props.data.mac.toUpperCase()}</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                <Paper className={classes.root}>
                <Table className={classes.table}>
                    <TableBody>
                    {rows.map(row => (
                        <TableRow key={row.name}>
                        <TableCell component="th" scope="row">
                            {row.name}
                        </TableCell>
                        <TableCell align="right">{row.value}</TableCell>
                        </TableRow>
                    ))}
                    <RingSettings showData = {true} side="Left" data={this.props.data.left_ring}/>
                    <RingSettings showData = {true} side="Right" data={this.props.data.right_ring}/>
                    </TableBody>
                </Table>
                <TextField
                    id="node_name"
                    defaultValue={this.state.node_name}
                    label="Set Name"
                    className={classes.textField}
                    onChange={this.handleOnChange}
                    margin="normal"
                />

                <TextField
                    defaultValue={this.state.led_len_left}
                    id="led_len_left"
                    label="Set Left Length"
                    className={classes.textField}
                    onChange={this.handleOnChange}
                    type="number"
                    step="1"
                    margin="normal"
                />

                <FormControl className={classes.formControl}>
                  <InputLabel id="channel_order_label">Channel Order</InputLabel>
                  <Select
                    value={this.state.channel_order}
                    onChange={this.handleOnChangeSelect("channel_order")}
                  >
                    {channelOrderItems}
                  </Select>
                </FormControl>


                <TextField
                    defaultValue={this.state.left_panel_height}
                    id="left_panel_height"
                    label="Set Left Panel Height"
                    className={classes.textField}
                    onChange={this.handleOnChange}
                    type="number"
                    step="1"
                    margin="normal"
                />

                <TextField
                    defaultValue={this.state.left_panel_width}
                    id="left_panel_width"
                    label="Set Left Panel Width"
                    className={classes.textField}
                    onChange={this.handleOnChange}
                    type="number"
                    step="1"
                    margin="normal"
                />

                <TextField
                    defaultValue={this.state.led_len_right}
                    id="led_len_right"
                    label="Set Right Length"
                    className={classes.textField}
                    onChange={this.handleOnChange}
                    type="number"
                    step="1"
                    margin="normal"
                />

                  <TextField
                    defaultValue={this.state.right_panel_height}
                    id="right_panel_height"
                    label="Set Right Panel Height"
                    className={classes.textField}
                    onChange={this.handleOnChange}
                    type="number"
                    step="1"
                    margin="normal"
                />

                <TextField
                    defaultValue={this.state.right_panel_width}
                    id="right_panel_width"
                    label="Set Right Panel Width"
                    className={classes.textField}
                    onChange={this.handleOnChange}
                    type="number"
                    step="1"
                    margin="normal"
                />
                <FormControl className={classes.formControl}>
                  <InputLabel id="left_color_order_label">Left Color Order</InputLabel>
                  <Select
                    value={this.state.left_color_order}
                    onChange={this.handleOnChangeSelect("left_color_order")}
                  >
                    {colorOrderItems}
                  </Select>
                </FormControl>

                <FormControl className={classes.formControl}>
                  <InputLabel id="right_color_order_label">Right Color Order</InputLabel>
                  <Select
                    value={this.state.right_color_order}
                    onChange={this.handleOnChangeSelect("right_color_order")}
                  >
                    {colorOrderItems}
                  </Select>
                </FormControl>


                <FormControl className={classes.formControl}>
                  <InputLabel id="local_mode_label">Local Mode</InputLabel>
                  <Select
                    value={this.state.local_mode}
                    onChange={this.handleOnChangeSelect("local_mode")}
                  >
                    {modeitems}
                  </Select>
                </FormControl>

                <FormControl className={classes.formControl}>
                  <InputLabel id="light_type_label">Light Type</InputLabel>
                  <Select
                    value={this.state.light_type}
                    onChange={this.handleOnChangeSelect("light_type")}
                  >
                    {lightTypeItems}
                  </Select>
                </FormControl>

                <FormControlLabel
                  control={
                    <Checkbox
                    id="is_mode_global"
                    onChange={this.handleOnChangeCheckbox}
                    color = "primary"
                    checked={!!this.state.is_mode_global}
                  />
                  }
                  label="Global Mode"
                />
                <RingSettings publisher = {this.publishState} 
                              ref={this.left_ring_state}
                              showData = {false} 
                              side="Left" 
                              data={this.props.data.left_ring}/>

                <RingSettings publisher = {this.publishState} 
                              ref={this.right_ring_state}
                              showData = {false} 
                              side="Right" 
                              data={this.props.data.right_ring}/>

                </Paper>
                </ExpansionPanelDetails>
            </ExpansionPanel>
          </div>
      );
    }
  }

  export default withStyles(styles)(NodeInfo);