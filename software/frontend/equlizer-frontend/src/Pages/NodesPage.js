import React, { Component } from "react";
import NodeInfo from "./NodeInfo"
import NetworkGraph from "./NetworkGraph";
import { withStyles } from '@material-ui/styles';
import MESSAGE_TYPES from './Types/MessageTypes'

const styles = theme => ({
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
});


class NodesPage extends Component {
    constructor(props, context) {
      super(props, context);
      this.handleData = this.handleData.bind(this);

      this.msgType = MESSAGE_TYPES["NODE_INFO"];
      
      props.socket.registerHandler(this.msgType, this.handleData);

      this.state = {
        nodes: []
      }
      this.receivedState = []
    }

    static getPageName()
    {
        return "Nodes";
    }

    handleData(data)
    {
        var cnt = data.readUInt32LE(0);
        data = data.slice(4, data.length);
        var nodes = [];
        for(let i = 0; i < cnt; i++)
        {
            var node = {};
            node.mac = data.slice(0, 6).toString('hex');
            node.parent_mac = data.slice(6, 12).toString('hex');

            node.time               = data.readDoubleLE(12);
            node.mean_air_time      = data.readFloatLE(20);
            var name_length         = data.readUInt8(24)
            node.node_name          = data.toString('ascii', 25, 45).substring(0, name_length);
            node.board_type         = data.readUInt8(45);
            node.strobe_offset      = data.readUInt32LE(46);
            node.led_len_left       = data.readUInt16LE(50);
            node.led_len_right      = data.readUInt16LE(52);
            node.local_mode         = data.readUInt8(54);
            node.is_mode_global     = data.readUInt8(55);
            node.layer              = data.readUInt8(56);
            node.left_panel_height  = data.readUInt8(57);
            node.left_panel_width   = data.readUInt8(58);
            node.right_panel_height = data.readUInt8(59);
            node.right_panel_width  = data.readUInt8(60);
            node.left_ring          = data.slice(61, 77);
            node.right_ring         = data.slice(81, 97);
            node.light_type         = data.readUInt8(101);
            node.left_color_order   = data.readUInt8(102);
            node.right_color_order  = data.readUInt8(103);
            node.fw_sync_progress   = data.readFloatLE(104);
            node.fs_sync_progress   = data.readFloatLE(108);
            node.channel_order      = data.readUInt8(112);

            nodes.push(node);
            data = data.slice(113, data.length);
        }
            this.setState({nodes: nodes});
    }

    handleChange(node, name, value)
    {
        var _node = this.state.nodes[node];
        _node[name] = value;        
    }

    handleNodeSetting(node)
    {

    }

      render()
    {
      const nodes = this.state.nodes;
      nodes.sort((a, b) => (a.layer > b.layer) ? 1 : (a.layer === b.layer) ? ((a.mac > b.mac) ? 1 : -1) : -1 )

      const node_elements = Object.keys(nodes).map((key, index) => {
        return(
              <NodeInfo key={nodes[key].mac} socket={this.props.socket} data={nodes[key]} msgType={this.msgType}/>
            );
        });

    return (
      <div>
       {node_elements}
        <hr />
          <NetworkGraph  nodes={this.state.nodes} />
      </div>
    );
    }
  }

  export default withStyles(styles)(NodesPage);