import React, { Component } from "react";
import FormControl from '@material-ui/core/FormControl';
import { withStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

import MESSAGE_TYPES from './Types/MessageTypes'


const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        width: '100%',
        margin: 'auto'
      },
      formControl: {
        margin: 'auto',
        width: '90%',
      },
      title: {
        flexGrow: 1,
      },    
  });

  
class PulseSettings extends Component {
    constructor(props, context) {
      super(props, context);
      this.handleData = this.handleData.bind(this);
      this.handleOnChange = this.handleOnChange.bind(this);
      this.handleOnChangeSlider = this.handleOnChangeSlider.bind(this);

      this.msgType = MESSAGE_TYPES["PULSE_SETTINGS"];
      props.socket.registerHandler(this.msgType, this.handleData);

      this.state = {
        pulse_time_ms   : 0,
        direction       : 0,
        single_shot     : false,
      }
    }

    static getPageName()
    {
        return "Pulse";
    }

    handleData(data)
    {
        this.setState({
            pulse_time_ms  : data.readUInt32LE(0),
            direction      : data.readInt8(4),
            single_shot    : Boolean(data.readUInt8(5)),
        });
    }

    handleOnChange = name => (event, checked) => 
    {
        var state = this.state;
        state["single_shot"] = checked;
        this.setState(state);
        this.publishState();
    }

    handleOnChangeSlider = name => (e, value) => {
        this.setState({
            [name]: value
        });
        this.publishState();
    };

    publishState()
    {
        var buf = new Buffer(10)
        buf.writeUInt8(this.msgType);
        buf.writeUInt32LE(this.state.pulse_time_ms, 1);
        buf.writeInt8(this.state.direction, 5);
        buf.writeUInt8(this.state.single_shot, 6);
        this.props.socket.publish(buf);
    }
      render()
    {
        const { classes } = this.props;
        return(
            <div>
            <Typography align="center" variant="h5">Pulse Settings</Typography>
            <form className={classes.root} autoComplete="off">
            <FormControl className={classes.formControl} margin='normal'>
                <Typography id="pulse-time-label" gutterBottom>
                    Pulse time (ms)
                </Typography>
                <Slider
                    value={this.state.pulse_time_ms}
                    onChange={this.handleOnChangeSlider("pulse_time_ms")}
                    onChangeCommitted={this.handleOnChangeSlider("pulse_time_ms")}
                    aria-labelledby="pulse-time-label"
                    min={0}
                    max={50000}
                    name='pulse_time_ms'
                    valueLabelDisplay="auto"    
                />           
                <Typography id="direction-label" gutterBottom>
                Direction
                </Typography>
                <Slider
                    value={this.state.direction}
                    onChange={this.handleOnChangeSlider("direction")}
                    onChangeCommitted={this.handleOnChangeSlider("direction")}
                    aria-labelledby="direction-label"
                    step={1}
                    min={-1}
                    max={1}
                    marks
                    name='direction'
                    valueLabelDisplay="auto"    
                />
                <FormControlLabel
                    control={
                    <Checkbox checked={this.state.random_color} onChange={this.handleOnChange('single_shoot')} value="1" />
                    }
                    label="Single shot"
                />
                </FormControl>
            </form>
            </div>
        );
    }
  }

  export default withStyles(styles)(PulseSettings);