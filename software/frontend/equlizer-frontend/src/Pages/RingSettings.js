import React, { Component } from "react";
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/styles';

const styles = theme => ({
    root: {
        width: '100%',
      },
      heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
      },
      table: {
        minWidth: 650,
      },
      textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200,
      },
  });


class RingSettings extends Component {
    constructor(props, context) {
      super(props, context);

      this.handleOnChange = this.handleOnChange.bind(this);
      
      this.rings = {}

      this.ring_size = props.data.readUInt8(0)

      this.state = {
        ring_size: this.ring_size,
      }
      
      for(let i = 0; i < 15; i++)
      {
        this.rings[i] = props.data.readUInt8(i + 1);
        this.state["ring_size_" + i] = this.rings[i]
      }
    }

    createData(name, value) {
        return { name, value };
    }

    handleOnChange(e, value)  {
        this.setState({
            [e.target.id]: e.target.value,
        }, this.props.publisher);
    };


    render()
    {
        const { classes } = this.props;

        var ringSettings = [];
        var rows = [
            this.createData(this.props.side + ' Ring Count', this.ring_size),
        ];

        for(let i = 0; i < this.ring_size && i < 14; i++)
        {
            rows.push(this.createData(this.props.side +" Ring " + i, this.rings[i]))
            ringSettings.push(
                <TextField
                    key={i}
                    defaultValue={this.rings[i]}
                    id={"ring_size_" + i}
                    label={"Set Ring " + i + " Count " + this.props.side}
                    className={classes.textField}
                    onChange={this.handleOnChange}
                    type="number"
                    step="1"
                    max="255"
                    min="0"
                    margin="normal"
                />
            )
        }

        if(this.props.showData)
        {
            return rows.map(row => (
                <TableRow key={row.name}>
                <TableCell component="th" scope="row">
                    {row.name}
                </TableCell>
                <TableCell align="right">{row.value}</TableCell>
                </TableRow>
            ));
        }
        else
        {
            return (
                <div>
                <TextField
                    defaultValue={this.state.ring_size}
                    id="ring_size"
                    label={"Set Ring Coount " + this.props.side}
                    className={classes.textField}
                    onChange={this.handleOnChange}
                    type="number"
                    step="1"
                    max="15"
                    min="0"
                    margin="normal"
                />
                {ringSettings}
                </div>
            );
        }
    }
}

export default withStyles(styles)(RingSettings);