import React, { Component } from 'react'
import CanvasJSReact from '../assets/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

class Plot extends Component {
	constructor(props) {
		super(props);
        this.updateChart = this.updateChart.bind(this);
        this.addData     = this.addData.bind(this);
        this.data = [{x: 1, y: 10}];   //dataPoints.
	}
	componentDidMount() {
        setInterval(this.updateChart, 200);
        this.props.parentObj.registerHandler(this.props.title, this.addData);
    }
    addData(data)
    {
        if(data.length === 0)
        {
            return;
        }
        var plot = this;
        data.forEach(function (item, index)
        {
            plot.data.push({x: plot.data[plot.data.length-1].x + 1, y: item});
            if(plot.data.length > 5000)
            {
                plot.data.shift();
            }
        });
    }
	updateChart() {
        if(this.chart) this.chart.render();
	}
	render() {
		const options = {
			title :{
				text: this.props.title
			},
			data: [{
				type: "line",
				dataPoints : this.data
			}]
		}
		return (
		<div>
			<CanvasJSChart options = {options}
				 onRef={ref => this.chart = ref}
			/>
		</div>
		);
	}
}

export default class SamplePlot extends React.Component {
    constructor(props) {
      super(props);

      this.handleData = this.handleData.bind(this);
      this.msgType = 5;
      this.state = {
          right_samples : [],
          left_samples  : [],
      }
      props.socket.registerHandler(this.msgType, this.handleData);  
      this.msgHandlers = [];
    }

    registerHandler(type, handler)
    {
        console.log("Registering handler for type " + type  )
        this.msgHandlers[type] = handler;
    }

    handleData(data)
    {
        var left_cnt  = data.readUInt32LE(0);
        var right_cnt = data.readUInt32LE(4);
        if(data.length < 8 + (left_cnt + right_cnt) * 4 )
        {
            return;
        }
        var right_samples = [];
        var left_samples  = [];
        for(let i = 0; i < right_cnt; i++)
        {
            right_samples[i] = data.readFloatLE(8 + i * 4);
        }
        for(let i = 0; i < left_cnt; i++)
        {
            left_samples[i] = data.readFloatLE(8 + (i + right_cnt) * 4);
        }
        if(this.msgHandlers["LEFT"])
        {
            this.msgHandlers["LEFT"](left_samples);
        }
        if(this.msgHandlers["RIGHT"])
        {
            this.msgHandlers["RIGHT"](right_samples);
        }
    }

    static getPageName()
    {
        return "SamplePlots";
    }
    render()
    {
        return(
            <div>
                <Plot title="LEFT"  parentObj = {this}/>
                <Plot title="RIGHT" parentObj = {this}/>
            </div>
        );
    }
  }
