import React, { Component } from "react";
import FormControl from '@material-ui/core/FormControl';
import { withStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';

import MESSAGE_TYPES from './Types/MessageTypes'


const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        width: '100%',
        margin: 'auto'
      },
      formControl: {
        margin: 'auto',
        width: '90%',
      },
      title: {
        flexGrow: 1,
      },    
  });

  
class StrobePage extends Component {
    constructor(props, context) {
      super(props, context);
      this.handleData = this.handleData.bind(this);

      this.msgType = MESSAGE_TYPES["STROBE"];
      
      props.socket.registerHandler(this.msgType, this.handleData);

      this.state = {
        on_time     : 0,
        off_time    : 0,
      }
    }

    static getPageName()
    {
        return "Strobe";
    }

    handleData(data)
    {
        this.setState({
            on_time  : data.readUInt32LE(0),
            off_time : data.readUInt32LE(4),
        });
    }

    handleOnChangeSlider = name => (e, value) => {
        this.setState({
            [name]: value * 1000000,
        });
        this.publishState();
    };

    publishState()
    {
        var buf = new Buffer(9)
        buf.writeUInt8(this.msgType, 0);
        buf.writeUInt32LE(this.state.on_time, 1);
        buf.writeUInt32LE(this.state.off_time, 5);
        this.props.socket.publish(buf);
    }
      render()
    {
        const { classes } = this.props;
        return(
            <div>
            <Typography align="center" variant="h5">Strobe Mode Settings</Typography>
            <form className={classes.root} autoComplete="off">
            <FormControl className={classes.formControl} margin='normal'>
                <Typography id="off-label" gutterBottom>
                    Off Time
                </Typography>
                <Slider
                    value={this.state.off_time / 1000000}
                    onChange={this.handleOnChangeSlider("off_time")}
                    onChangeCommitted={this.handleOnChangeSlider("off_time")}
                    aria-labelledby="off-label"
                    step={0.01}
                    min={0.01}
                    max={1}
                    marks
                    name='off_time'
                    valueLabelDisplay="auto"    
                />           
                <Typography id="on-label" gutterBottom>
                    On Time
                </Typography>
                <Slider
                    value={this.state.on_time / 1000000}
                    onChange={this.handleOnChangeSlider("on_time")}
                    onChangeCommitted={this.handleOnChangeSlider("on_time")}
                    aria-labelledby="on-label"
                    step={0.01}
                    min={0.01}
                    max={1}
                    marks
                    name='on_time'
                    valueLabelDisplay="auto"    
                />
            </FormControl>
            </form>
            </div>
        );
    }
  }

  export default withStyles(styles)(StrobePage);