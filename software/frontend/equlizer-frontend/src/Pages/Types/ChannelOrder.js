const CHANNEL_ORDER = {
    0 : 'LEFT_RIGHT',
    1 : 'RIGHT_LEFT',
}

export default CHANNEL_ORDER;