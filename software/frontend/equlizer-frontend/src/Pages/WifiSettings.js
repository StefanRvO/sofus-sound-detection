import React, { Component } from "react";
import FormControl from '@material-ui/core/FormControl';
import { withStyles } from '@material-ui/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import AccessPointBox from './AccessPointBox'

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        width: 300,
      },
      formControl: {
        margin: 'auto',
        minWidth: 120,
      },
      title: {
        flexGrow: 1,
      },
      paper: {
        width: '100%',
        overflowX: 'auto',
      },

  });


  var sendIntervalFunc = null;
  
class WifiSettings extends Component {
    constructor(props, context) {
      super(props, context);
      this.handleData = this.handleData.bind(this);
      this.handleOnChangeCheckbox = this.handleOnChangeCheckbox.bind(this);
      this.sendScan               = this.sendScan.bind(this);

      this.msgType     = 7;
      this.scanMsgType = 8
      props.socket.registerHandler(this.msgType, this.handleData);

      this.state = {
        should_scan   : false,
        connected     : false,
        sta_ssid      : "",
        sta_pass      : "",
        ap_list       : [],
      }
    }

    static getPageName()
    {
        return "Wifi Settings";
    }

    sendScan()
    {
        var buf = new Buffer(2)
        buf.writeUInt8(this.scanMsgType, 0);
        buf.writeUInt8(this.state.should_scan, 1);
        this.props.socket.publish(buf);
    }

    componentWillUnmount()
    {
      clearInterval(sendIntervalFunc);
    }
    componentDidMount()
    {
      this.sendScan();
      sendIntervalFunc = setInterval(this.sendScan, 1000);
    }


    createData(name, value) {
      return { name, value };
    }

    handleData(data)
    {
        var ap_list = []
        var cnt       = data.readUInt16LE(0);
        var connected = data.readUInt8(2);
        var ssid_len  = data.readUInt8(3);
        var pass_len  = data.readUInt8(4);
        var sta_ssid  = data.toString('ascii', 5, 37).substring(0, ssid_len);
        var sta_pass  = data.toString('ascii', 37, 101).substring(0, pass_len);
        data = data.slice(101, data.length)
        for(let i = 0; i < cnt; i++)
        {
          var node = {}
          node.bssid    = data.slice(0, 6).toString('hex');
          node.ssid_len = data.readUInt8(6);
          node.ssid     = data.toString('ascii', 7, 40).substring(0, node.ssid_len);
          node.channel  = data.readUInt8(40);
          node.second_channel = data.readUInt8(41);
          node.rssi           = data.readInt8(42);
          node.authmode       = data.readUInt8(43);
          data = data.slice(44, data.length);
          ap_list[i] = node;
        }
        this.setState({"ap_list"   : ap_list});
        this.setState({"sta_ssid"  : sta_ssid});
        this.setState({"sta_pass"  : sta_pass});
        this.setState({"connected" : connected});
      }

    
    handleOnChangeCheckbox(e, value)  {
      if(e.target.id === "should_scan") {
        this.setState({
          [e.target.id]: value,
        });
      }
    };


    render()
    {
      const { classes } = this.props;
      const rows = [
        this.createData('SSID', this.state.sta_ssid),
        this.createData('Password', this.state.sta_pass),
        this.createData('Connected', this.state.connected),
      ];

      const accesPoints = this.state.ap_list;

      const aps = Object.keys(accesPoints).map((key, index) => {
        return(
                <AccessPointBox key={index} socket={this.props.socket} data={accesPoints[index]} msgType={this.msgType}/>
            );
        });

      return(
          <div>
          <form className={classes.root} autoComplete="off">
          <FormControl className={classes.formControl} margin='normal'>
              <FormControlLabel
                control={
                  <Checkbox
                  id="should_scan"
                  onChange={this.handleOnChangeCheckbox}
                  color = "primary"
                  checked={!!this.state.should_scan}
                />
                }
                label="Enable wifi scanning"
              />
            </FormControl>
            </form>

          <Typography className={classes.heading}>Status</Typography>
          <Paper className={classes.paper}>
            <Table className={classes.table}>
                <TableBody>
                {rows.map(row => (
                    <TableRow key={row.name}>
                    <TableCell component="th" scope="row">
                        {row.name}
                    </TableCell>
                    <TableCell align="right">{row.value}</TableCell>
                    </TableRow>
                ))}
                </TableBody>
            </Table>
          </Paper>
          <br />
          <AccessPointBox key = "____custom____" socket={this.props.socket} msgType={this.msgType} data={{}} />
          {aps}
          </div>
      );
    }
  }

  export default withStyles(styles)(WifiSettings);