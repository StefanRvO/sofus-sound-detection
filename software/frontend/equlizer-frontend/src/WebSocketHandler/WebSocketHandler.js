import React from 'react';
import Websocket from 'react-websocket';
import * as blobtobuffer from "blob-to-buffer/index.js";

export default class WebSocketHandler extends React.Component {
    constructor(props) {
      super(props);
      this.msgHandlers = [];
      this.sendPing    = this.sendPing.bind(this);
      setInterval(this.sendPing, 1000);
    }

    registerHandler(type, handler)
    {
        console.log("Registering handler for type " + type  )
        this.msgHandlers[type] = handler;
    }

    handleData(data)
    {
        const socketHandler = this;
        blobtobuffer(data, function(err, buffer) {
            if(err) throw err
            var msg_type = buffer[0];
            if(socketHandler.msgHandlers[msg_type])
            {
                socketHandler.msgHandlers[msg_type](buffer.slice(1, buffer.length)); 
            }
        });
    }
    handleOnOpen()
    {

    }

    handleOnClose()
    {

    }

    publish(buffer)
    {
        try {
            this.refWebSocket.sendMessage(buffer);
        } catch(error) {

        }
    }

    sendPing()
    {
        var buf = new Buffer(1);
        buf.writeUInt8(10);
        this.publish(buf);
    }
    getWSHost()
    {
        if(process.env.NODE_ENV)
        {
            return "192.168.201.154";
        }
        return window.location.hostname
    }
    render()
    {
        return(
            <div>
                <Websocket url= {'ws://' + this.getWSHost()}
                    onMessage={this.handleData.bind(this)}
                    onOpen={this.handleOnOpen.bind(this)}
                    onClose={this.handleOnClose.bind(this)}
                    reconnectIntervalInMilliSeconds={2}
                    ref ={Websocket => {
                        this.refWebSocket = Websocket;
                    }}/>
            </div>
        );
    }
  }
  